module Hyperbolic where

import Data.Complex
import Vector

newtype H4 = H4{ unH4 :: V4 }
newtype H2 = H2{ unH2 :: V2 }
newtype H = H{ unH :: R }

hdist :: H2 -> H2 -> H
hdist (H2 z) (H2 w) = H $ acosh (1 + 2 * norm2 (z ^-^ w) / ((1 - norm2 z) * (1 - norm2 w)))

h2e1 :: H -> R
h2e1 (H z) = sqrt ((cosh z - 1) / (cosh z + 1))

h2e2 :: H2 -> V2
h2e2 h@(H2 v) = norm v ^* h2e1 (hdist h (H2 o))
e2h1 :: R -> H
e2h1 z = hdist (H2 (V2 z 0)) (H2 o)

e2h2 :: V2 -> H2
e2h2 z = H2 (norm z ^* (unH $ e2h1 (sqrt (norm2 z))))

moebius :: H2 -> H4
moebius (H2 (V2 x y)) = H4 (V4 x y 1 0)

unmoebius :: H4 -> H2
unmoebius (H4 (V4 a b c d)) = let (x:+y) = (a:+b)/(c:+d) in H2 (V2 x y)

mlength :: H4 -> R
mlength (H4 (V4 a b c d)) = magnitude (a:+b) / magnitude (c:+d)

rotation :: R -> M4
rotation a = M4 c (-s) 0 0  s c 0 0  0 0 1 0  0 0 0 1
  where
    c = cos a
    s = sin a

translation :: H2 -> M4
translation (H2 (V2 x y))
  | l > 0 = rotation (-s) ^^*^^ m ^^*^^ rotation s
  | otherwise = m
  where
    m = M4 f 0 g 0  0 f 0 g  g 0 f 0  0 g 0 f
    s = phase (x:+y)
    l = magnitude (x:+y)
    e = exp l
    f = e + 1
    g = e - 1

rotationAbout :: H2 -> R -> M4
rotationAbout z@(H2 (V2 x y)) a = translation z ^^*^^ rotation a ^^*^^ translation (H2 (V2 (-x) (-y)))

ecircle :: H2 -> H -> (V2, R)
ecircle c@(H2 (V2 cx cy)) (H hr) = (ec, er)
  where
    p = phase (cx :+ cy)
    a = unmoebius . H4 $ translation (H2 (V2 (-hr * cos p) (-hr * sin p))) ^^*^ unH4 (moebius c)
    b = unmoebius . H4 $ translation (H2 (V2 ( hr * cos p) ( hr * sin p))) ^^*^ unH4 (moebius c)
    ea = h2e2 a
    eb = h2e2 b
    ec = (ea ^+^ eb) ^* 0.5
    er = 0.5 * (sqrt $ norm2 (ea ^-^ eb))
