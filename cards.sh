#!/bin/bash
pngtopnm < edge.png > edge.pgm
pngtopnm < back.png > back.pgm
mkdir 4x1
ls snowglobe-*.pgm | sort -R | xargs -n 4 pnmcat -lr | ( cd 4x1 ; pnmsplit )
mkdir 4x6
ls 4x1/* | xargs -n 6 pnmcat -tb | ( cd 4x6 ; pnmsplit )
mkdir 5x7
cd 4x6
for i in *
do
  pnmcat -tb -black ../edge.pgm $i ../edge.pgm |
  pgmtoppm white-black | ppmtopgm | pnmgamma 0.25 |
  pnmscale -xysize 1500 2100 | pnmcat -lr ../back.pgm - |
  pnmtopng -force -interlace -phys 11811 11811 1 > ../5x7/$i.png
done
cd ../5x7
ls -1sh
